mod utils;

use std::collections::VecDeque;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct StepDetector {
    window: VecDeque<f64>,
    mean: f64,
    variance: f64,
    step_count: u32,
    size: usize,
    threshold: f64,
}

#[wasm_bindgen]
impl StepDetector {
    pub fn new(size: usize, threshold: f64) -> Self {
        StepDetector {
            window: VecDeque::with_capacity(size),
            mean: 0.0,
            variance: 0.0,
            step_count: 0,
            size,
            threshold,
        }
    }

    pub fn add_data(&mut self, data: f64) {
        // Add the new data to the window
        if self.window.len() == self.size {
            self.window.pop_front();
        }
        self.window.push_back(data);

        // Recalculate mean and variance
        let sum: f64 = self.window.iter().sum();
        self.mean = sum / self.window.len() as f64;
        self.variance = self
            .window
            .iter()
            .map(|x| (x - self.mean).powi(2))
            .sum::<f64>()
            / self.window.len() as f64;

        // Detect steps
        let deviation = (data - self.mean).abs();
        if deviation > self.threshold * self.variance.sqrt() {
            self.step_count += 1;
        }
    }

    pub fn step_count(&self) -> u32 {
        self.step_count
    }
}
