ARG RUST_IMAGE_TAG=1.67
FROM rust:$RUST_IMAGE_TAG

RUN apt-get update \
    && apt-get install -y libnss3

RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh \
    && wasm-pack --help
