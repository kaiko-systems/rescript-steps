# Steps Counter Based on Accelerometer

## Usage

Experimental package to continuously count steps using accelerometer data.  The (intended) is API quite simple:

```js
const WINDOW_SIZE = 100
const THRESHOLD = 0.6
let counter = new StepCounter(WINDOW_SIZE, THRESHOLD)

let report = (counter) => console.log("Steps detected", counter)
counter.start(report)
```

*Note*: Safari requires to ask for permission to use the `DeviceMotionEvent` API, so you need request the permission before calling `counter.start` above.

## License

Licensed under MIT license ([LICENSE](LICENSE) or http://opensource.org/licenses/MIT).
